import java.util.*;

class Calculator {

    static List<String> operatorKalkulator = new ArrayList();

    static void parsingInput (String inputUser) {
        String temporaryInputContainer = "";
        for (int i = 0; i < inputUser.length(); i++) {
            if(inputUser.charAt(i) == '+' || inputUser.charAt(i) == '-' || inputUser.charAt(i) == '*' || inputUser.charAt(i) == '/') {
                operatorKalkulator.add(temporaryInputContainer);
                operatorKalkulator.add(String.valueOf(inputUser.charAt(i)));

                temporaryInputContainer = "";
            } else {
                temporaryInputContainer = temporaryInputContainer + String.valueOf(inputUser.charAt(i));
            }

            if (i == inputUser.length() - 1) {
                operatorKalkulator.add(temporaryInputContainer);
                temporaryInputContainer = "";
            }
        }
    }

    public static double penjumlahan (double result, int i) {
        return result + Double.parseDouble(operatorKalkulator.get(i));
    }

    public static double pengurangan (double result, int i) {
        return result - Double.parseDouble(operatorKalkulator.get(i));
    }

    public static double perkalian (double result, int i) {
        return result * Double.parseDouble(operatorKalkulator.get(i));
    }

    public static double pembagian (double result, int i) {
        return result / Double.parseDouble(operatorKalkulator.get(i));
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input number and operators that you want to calculate: ");

        try {
            String inputUser = input.next();

            parsingInput(inputUser);

            double hasil = 0;
            for (int i = 0; i < operatorKalkulator.size(); i++) {
                switch (operatorKalkulator.get(i)) {
                    case "+":
                        i++;
                        hasil = penjumlahan(hasil, i);
                        break;
                    case "-":
                        i++;
                        hasil = pengurangan(hasil, i);
                        break;
                    case "*":
                        i++;
                        hasil = perkalian(hasil,i);
                        break;
                    case "/":
                        i++;
                        hasil = pembagian(hasil,i);
                        break;
                    default:
                        hasil = Double.parseDouble(operatorKalkulator.get(i));
                }
            }

            System.out.println("** Result: " + hasil);

        } catch (InputMismatchException e) {
            System.out.println("Invalid input");
        }
    }
}
